public class Card{
	private String ranks;
	private String suits;
		// constructor
	public Card(String ranks, String suits){
		this.ranks = ranks;
		this.suits = suits;
	}
	//Getters
	public String getRank(){
		return this.ranks;
	}
	public String getSuit(){
		return this.suits;
	}
	//toString
	public String toString(){
		return "The card is " + this.ranks + " of " + this.suits;
	}
}