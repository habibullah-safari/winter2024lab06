public class GameManager{
	private Deck drawPie;
	private Card centerCard;
	private Card playerCard;
	
	//constructor
	public GameManager(){
		Deck newDeck = new Deck();
		this.drawPie = newDeck;
		drawPie.shuffle();
		this.centerCard = drawPie.drawTopCard();
		this.playerCard = drawPie.drawTopCard();	
	}
	//toString 
	public String toString(){
		String str =
		"------------------------ " + "\n" +	
		"Center Card: " + this.centerCard + "\n" + "Player Card: " + this.playerCard + "\n" + 
		"------------------------";
		return str;
	}
	// custom methods 
	public  void dealCards(){
		drawPie.shuffle();
		this.centerCard = drawPie.drawTopCard();
		this.playerCard = drawPie.drawTopCard();
	}
	public int getNumberOfCards(){
		return this.drawPie.length();
	}
	public int calculatePoints(){
		int pointCounter = 0; 
	/* 4 if same 
			2 if suits are the same
			-1 if if none of them are the same 
		*/
		
		String compareCard = this.centerCard.getRank();
		String compareCard2 = this.playerCard.getRank();
		
		String compareSuit = this.centerCard.getSuit(); 
		String compareSuit2 = this.playerCard.getSuit();
		
		if( compareCard.equals(compareCard2)){
			pointCounter = 2;
		}else if (compareSuit.equals(compareSuit2)){
			pointCounter = 4;
		}else{
			pointCounter = -1;
		}
		return pointCounter;
	}
}