public class LuckyCardGameApp{
	public static void main(String [] args){	
		GameManager manager = new GameManager();
		int totalPoint = 0 ; 
	    System.out.println(" Welcome to Lucky! Card game ");
		System.out.println(manager);		
			
		int counter = 1;
		while (manager.getNumberOfCards() > 1 && totalPoint<= 5){
			System.out.println(" Round : " + counter);
			System.out.println(manager);
			totalPoint += manager.calculatePoints();
			System.out.println("Your points after this round " + totalPoint + "\n" + 
			"****************************************");
			manager.dealCards();
				counter++;
		}
		
		System.out.println(totalPoint);
		
		if(totalPoint >= 5){
			System.out.println("congrats you won");
			}else{
				System.out.println("Sorry you lost");
			}
	}		
}