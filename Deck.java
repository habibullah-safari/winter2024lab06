import java.util.Random; 
public class Deck{
	private Card[] cards;
	private int numberOfCards ;
	private Random rng;
	
	//constructor
	public Deck(){
		Card [] cardArray = new Card[52];
		this.cards = cardArray; 
		this.numberOfCards =52;
		this.rng = new Random(); 
		
		String [] suitsArray = {"Hearts", "Diamonds", "Spades", "Clubs"};
		String [] ranksArray = { "Ace", "two", "three ","four", "five", "six", "seven","eight",
		"nine", "ten", "Jack", "Queen", "King"};
		 
			int counter =0;
			for(int index =0; index < suitsArray.length; index++){	
				for (int j = 0; j < ranksArray.length; j++){
					this.cards[counter] = new Card( ranksArray[j], suitsArray[index]);	
					counter++;
				}
			}	
		for(int index =0; index < cards.length; index++){
			System.out.println(cards[index]);
		}
	}
	//Getters 
	public Card[] getCards(){
		return this.cards;
	}
	public int getNumberOfCards(){
		return this.numberOfCards;
	}
	//length method	
	public int length(){
		return this.numberOfCards;
	}
	// Draw a card
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	// To String 
	public String toString(){			
		String allCards= ""; 
		for(int index =0 ; index < this.numberOfCards; index++){
			 allCards += cards[index] + "\n";  
		}
	return allCards;
	}
	// returns a String which contains all the cards inside the deck
	//"\n" makes new 
	// Shuffle method
	public void shuffle(){
		//Random randNum = new Random();
		for(int index = 0; index < this.numberOfCards; index++){
			int randomnumber = rng.nextInt(numberOfCards) ;
			Card swap = this.cards[index];
			this.cards[index] = this.cards[randomnumber]; 
			this.cards[randomnumber] = swap;
		}
	}
}